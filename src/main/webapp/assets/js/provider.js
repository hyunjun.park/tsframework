define( [], function () {
'use strict'
function ProviderFactory () {
    this.$provide = null;
    this.$compileProvider = null;
    this.$controllerProvider = null;
    this.$filterProvider = null;
    this.$urlRouterProvider = null;
    this.$stateProvider = null;
    this.$filter = null;
}
ProviderFactory.prototype.setProvide = function ( $provide ) { this.$provide = $provide; };
ProviderFactory.prototype.setCompileProvider = function ( $compileProvider ) { this.$compileProvider = $compileProvider; };
ProviderFactory.prototype.setControllerProvider = function ( $controllerProvider ) { this.$controllerProvider = $controllerProvider; };
ProviderFactory.prototype.setFilterProvider = function ( $filterProvider ) { this.$filterProvider = $filterProvider; };
ProviderFactory.prototype.setUrlRouterProvider = function ( $urlRouterProvider ) { this.$urlRouterProvider = $urlRouterProvider; };
ProviderFactory.prototype.setStateProvider = function ( $stateProvider ) { this.$stateProvider = $stateProvider; };
ProviderFactory.prototype.controller = function ( name, provider ) { this.$controllerProvider.register( name, provider ); return this; };
ProviderFactory.prototype.directive = function ( name, provider ) { this.$compileProvider.directive( name, provider ); return this; };
ProviderFactory.prototype.filter = function ( name, filter ) { this.$filterProvider.register( name, filter ); return this; };
ProviderFactory.prototype.factory = function ( name, factory ) { this.$provide.factory( name, factory ); return this; };
ProviderFactory.prototype.service = function ( name, service ) { this.$provide.service( name, service ); return this; };
ProviderFactory.prototype.constant = function ( name, constant ) { this.$provide.constant( name, constant ); return this; };
ProviderFactory.prototype.value = function ( name, value ) { this.$provide.value( name, value ); return this; };
return new ProviderFactory();
} );
