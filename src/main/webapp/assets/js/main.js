( function ( window, requirejs, config ) {
'use strict';
if ( !config ) {
    return;
}
var
n = window.navigator,
l = n.browserLanguage || n.languages && n.languages[ 0 ];

requirejs.onError = function ( err ) {
    if ( err.requireType === 'timeout' ) {
        alert( '타임아웃이 발생하였습니다.' );
    } else {
        throw err;
    }
};

requirejs.config( {
    waitSeconds: 60,
    baseUrl: 'assets/js',
    paths: {
        'jquery':                  'lib/jquery/1.12.3/jquery.min',
        'bootstrap':               'lib/bootstrap/3.4.1/js/bootstrap.min',
        'angular':                 'lib/angular/1.7.4/angular.min',
        'angular-cookies':         'lib/angular/1.7.4/angular-cookies.min',
        'angular-animate':         'lib/angular/1.7.4/angular-animate.min',
        'angular-aria':            'lib/angular/1.7.4/angular-aria.min',
        'angular-messages':        'lib/angular/1.7.4/angular-messages.min',
        'angular-i18n':            'lib/angular/1.7.4/i18n/angular-locale_' + l.toLowerCase(),
        'angular-material':        'lib/angular-material/1.1.19/angular-material.min',
        'angular-ui-takesolution': 'libx/angular-ui-takesolution',
        
        'knockout-latest': 		   'lib/devExtreme/js/knockout-latest',
        'devExtreme-all': 		   'lib/devExtreme/js/dx.all',
        'devExtreme': 		   	   'libx/devExtreme',
        
        'realgridjs-lic': 		   'lib/realgrid/1.1.34/realgridjs-lic',
        'realgridjs_eval': 		   'lib/realgrid/1.1.34/realgridjs_eval.1.1.34.min',
        'realgridjs-api': 		   'lib/realgrid/1.1.34/realgridjs-api.1.1.34',
        'jszip': 		           'lib/realgrid/1.1.34/jszip.min',
        'realgrid': 			   'libx/realgrid',
        
        'provider':                'provider',
        'login':                   'login',
        'index':                   'index'
    },
    shim: {
        'angular':                 { deps: [ 'jquery' ], exports: 'angular' },
        'angular-cookies':         { deps: [ 'angular' ] },
        'angular-i18n':            { deps: [ 'angular' ] },
        'angular-messages':        { deps: [ 'angular' ] },
        'angular-aria':            { deps: [ 'angular' ] },
        'angular-animate':         { deps: [ 'angular' ] },
        'angular-material':        { deps: [ 'angular', 'angular-animate', 'angular-aria', 'angular-messages' ] },
        'angular-ui-bootstrap':    { deps: [ 'angular', 'bootstrap' ]},
        
        'angular-ui-takesolution': { deps: [ 'angular', 'angular-i18n', 'angular-material', 'provider'] },
        
        'devExtreme': { deps: [ 'angular', 'knockout-latest', 'devExtreme-all', 'provider'] },
        
        'realgridjs-api':         { deps: [ 'realgridjs_eval' ], exports: 'realgridjs-api' },
        'realgrid':				   { deps: [ 'realgridjs-lic', 'realgridjs-api', 'jszip' ]},
        
        'provider':                { deps: [ 'angular' ] },
        
        'index':                   { deps: [ 'angular-ui-takesolution', 'realgrid'] },
        'login':                   { deps: [ 'angular-ui-takesolution' ] },
        'test':					   { deps: [ 'devExtreme' ] }
    }
} );

requirejs( [
    'angular',
    config.moduleName
], function ( angular ) {
    angular.element( function () {
        angular.bootstrap( this, [ config.moduleName ] );
    } );
} );

} )( window, window.requirejs, window.config );
