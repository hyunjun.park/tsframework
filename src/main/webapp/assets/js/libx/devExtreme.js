angular.module( 'devExtreme', [] )
.directive( 'unicode', function ( $parse ) {
    return {
        priority: 2,
        restrict: 'A',
        compile: function ( element ) {
            element.on( 'compositionstart', function ( e ) {
                e.stopImmediatePropagation();
            } );
        },
    };
} );
