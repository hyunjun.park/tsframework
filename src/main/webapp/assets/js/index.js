define( [
    'angular',
    'provider',
    '/session'
], function ( angular, provider, session ) {
'use strict';
return angular.module( 'index', [ 'ngMaterial', 'ui.takesolution' ] )
.config( function ( $provide, $compileProvider, $controllerProvider, $filterProvider ) {
    provider.setProvide( $provide );
    provider.setCompileProvider( $compileProvider );
    provider.setControllerProvider( $controllerProvider );
    provider.setFilterProvider( $filterProvider );
    $compileProvider.debugInfoEnabled( false );
} )
.controller( 'TsuIndexController', function ( $scope, $window, $compile, $element ) {
    $scope.tabs = [];

    $scope.menus = session.details.menus;

    $scope.selectMenu = function ( menu ) {
        var tabs = $scope.tabs,
              id = menu.menuId;

        var isSelected = false;
        for ( var i = 0; i < tabs.length; i++ ) {
            var tab = tabs[ i ];
            if ( tab.id === id ) {
                isSelected = tab.isActive = true;
            } else {
                tab.isActive = false;
            }
        }
        // 선택된 메뉴가 없는지?
        if ( !isSelected ) {
            tabs.push( {
                id: menu.menuId,
                name: menu.menuNm,
                isActive: true
            } );

            var directiveName = id.toLowerCase();
            // requireJS를 사용하여 js 파일을 동적으로 로드한다.
            require( [ '/assets/web/' + directiveName + '/' + directiveName + '.js' ], function () {
                var content = $element.find( '#' + id ).attr( directiveName, '' );
                $compile( content )( $scope );
                $scope.$apply();
            } );
        }
     };

    $scope.logout = function () {
        $window.location.href = '/logout';
    };
} )
.directive( 'tsuIndex', function () {
    return {
        restrict: 'E',
        scope: {
            title: '@'
        },
        controller: 'TsuIndexController',
        templateUrl: 'assets/js/tpls/tsu-index.html',
        link: function ( scope, element, attrs, ctrl ) {}
    };
} );

} );
