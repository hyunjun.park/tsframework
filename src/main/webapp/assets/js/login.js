define( [
    'angular',
    'provider'
], function ( angular, provider ) {
'use strict';
return angular.module( 'login', [ 'ngMaterial', 'ui.takesolution' ] )
.config( function ( $provide, $compileProvider, $controllerProvider, $filterProvider ) {
    provider.setProvide( $provide );
    provider.setCompileProvider( $compileProvider );
    provider.setControllerProvider( $controllerProvider );
    provider.setFilterProvider( $filterProvider );
    $compileProvider.debugInfoEnabled( false );
} )
.controller( 'TsuLoginController', function ( $scope, $window, $http ) {
    $scope.user = { p: '하드코딩아이디', c: '하드코딩비밀번호' };

    $scope.login = function () {
        $http.post( '/login/auth', $scope.user ).then( function ( response ) {
            $window.location.href = '/';
        }, function ( error ) {
            console.warn( error );
        } );
    };
} )
.directive( 'tsuLogin', function () {
    return {
        restrict: 'E',
        scope: {
            title: '@'
        },
        controller: 'TsuLoginController',
        templateUrl: 'assets/js/tpls/tsu-login.html',
        link: function ( scope, element, attrs, ctrl ) {}
    };
} );

} );
