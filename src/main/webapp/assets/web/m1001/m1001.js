define( [
    'angular',
    'provider'
], function ( angular,provider ) {
'use strict';
provider
.controller( 'M1001Controller', function ( $scope, $http ) {
    let dataProvider = new RealGridJS.LocalDataProvider();
	let gridView = new RealGridJS.GridView("realgrid");
	gridView.setDataSource(dataProvider);

	$scope.obj = {name:'vi',value:'va'};
	$scope.re = '';
	$scope.title = '첫번째 메뉴입니다.';
	
	$scope.ajax = function (type, url, params) {
		let obj =  { method : type, url : url, data : params};
		
		if(type==="POST"){
			obj.headers = {'Content-type' : 'application/json'};
		}

		return new Promise(function (resolve, reject) {
				let res = $http(obj).then(
					function(res){
						return res.data;
					},function(rea){
						return rea;
					}
				)
				resolve(res);
		});
		
		
	};
	
	$scope.test = function () {
		$scope.ajax( 'POST', '/cms001/test', $scope.obj ).then(function(res){
			console.log('11',res);
		});
		
    };
	
    
} )
.directive( 'm1001', function () {
    return {
        scope: {},
        controller: 'M1001Controller',
        templateUrl: '/assets/web/m1001/m1001.html',
        link: function ( scope ) {
            console.log( scope );
        }
    };
} );

} );
