define( [
    'provider'
], function ( provider ) {
'use strict';
provider
.controller( 'M1003Controller', function ( $scope ) {
    $scope.title = '세번째 메뉴입니다.';
} )
.directive( 'm1003', function () {
    return {
        scope: {},
        controller: 'M1003Controller',
        templateUrl: '/assets/web/m1003/m1003.html'
    };
} );

} );
