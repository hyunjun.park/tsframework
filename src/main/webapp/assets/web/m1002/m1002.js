define( [
    'provider'
], function ( provider ) {
'use strict';
provider
.controller( 'M1002Controller', function ( $scope ) {
    $scope.title = '두번째 메뉴입니다.';
} )
.directive( 'm1002', function () {
    return {
        scope: {},
        controller: 'M1002Controller',
        templateUrl: '/assets/web/m1002/m1002.html'
    };
} );

} );
