<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="include/include-meta.jsp" %>
<%@ include file="include/include-css.jsp" %>
<script>window.config = { moduleName: 'login' };</script>
<%@ include file="include/include-js.jsp" %>
</head>
<body>
<tsu-login></tsu-login>
</body>
</html>
