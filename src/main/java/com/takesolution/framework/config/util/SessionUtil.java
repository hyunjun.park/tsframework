package com.takesolution.framework.config.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class SessionUtil {
    public static SecurityContext getContext () {
        return SecurityContextHolder.getContext();
    }

    public static Authentication getAuthentication () {
        return getContext().getAuthentication();
    }

    /*
    public static LoginAuthentication getLoginAuthentication () {
        return (LoginAuthentication) getContext().getAuthentication();
    }

    public static LoginUserDetails getLoginUserDetails () {
        Authentication authentication = getAuthentication();
        return authentication != null ? ( LoginUserDetails ) authentication.getDetails() : null;
    }
    */

    public static boolean isLogin () {
        Authentication authentication = getContext().getAuthentication();
        if ( authentication != null && authentication.isAuthenticated() ) {
            return authentication.getAuthorities().stream().anyMatch( a -> {
                return "ROLE_ADMIN".equals( a.getAuthority() );
            } );
        }
        return false;
    }
}
