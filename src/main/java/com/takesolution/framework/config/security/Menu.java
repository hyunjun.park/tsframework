package com.takesolution.framework.config.security;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor( staticName = "of" )
public class Menu {
    private String menuId;
    private String menuNm;
}
