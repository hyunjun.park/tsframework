package com.takesolution.framework.config.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor( staticName = "of" )
public class LoginAuthentication implements Authentication {
    private static final long serialVersionUID = -7268283768120769784L;

    boolean authenticated = false;

    @NonNull
    private UserDetails details;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return details.getAuthorities();
    }

    @Override
    public Object getPrincipal () {
        return null;
    }

    @Override
    public Object getCredentials () {
        return null;
    }

    @Override
    public String getName () {
        return null;
    }

    @Override
    public Object getDetails () {
        return details;
    }

    @Override
    public boolean isAuthenticated () {
        return authenticated;
    }

    @Override
    public void setAuthenticated ( boolean isAuthenticated ) throws IllegalArgumentException {
        authenticated = isAuthenticated;
    }
}
