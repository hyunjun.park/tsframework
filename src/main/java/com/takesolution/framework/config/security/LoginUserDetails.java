package com.takesolution.framework.config.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
public class LoginUserDetails implements UserDetails {
    private static final long serialVersionUID = -6217715582606564968L;

    @Getter
    private String userId;

    private String pwd;

    @Getter
    @Setter
    private List<Menu> menus;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities () {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add( new SimpleGrantedAuthority( "ROLE_ADMIN" ) );
        return authorities;
    }

    @Override
    public String getUsername () {
        return userId;
    }

    @Override
    public String getPassword () {
        return pwd;
    }

    @Override
    public boolean isAccountNonExpired () {
        return true;
    }

    @Override
    public boolean isAccountNonLocked () {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired () {
        return true;
    }

    @Override
    public boolean isEnabled () {
        return true;
    }
}
