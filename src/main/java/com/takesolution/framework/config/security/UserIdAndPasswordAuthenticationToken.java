package com.takesolution.framework.config.security;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import lombok.Getter;

public class UserIdAndPasswordAuthenticationToken extends AbstractAuthenticationToken {
    private static final long serialVersionUID = -5870632402660806188L;

    @Getter
    private String userId;

    @Getter
    private String password;

    public UserIdAndPasswordAuthenticationToken ( String userId, String password ) {
        super( null );
        this.userId = userId;
        this.password = password;
    }

    private UserIdAndPasswordAuthenticationToken ( Collection<? extends GrantedAuthority> authorities ) {
        super( authorities );
    }

    @Override
    public Object getCredentials () {
        return userId;
    }

    @Override
    public Object getPrincipal () {
        return password;
    }
}
