package com.takesolution.framework.config.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class LoginAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private String defaultTartgetUrl;

    public LoginAuthenticationSuccessHandler ( String defaultTartgetUrl ) {
        this.defaultTartgetUrl = defaultTartgetUrl;
    }

    @Override
    public void onAuthenticationSuccess ( HttpServletRequest request, HttpServletResponse response, Authentication authentication ) throws IOException, ServletException {
        response.sendRedirect( defaultTartgetUrl );
    }
}
