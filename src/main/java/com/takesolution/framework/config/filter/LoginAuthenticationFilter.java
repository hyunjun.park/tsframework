package com.takesolution.framework.config.filter;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.core.type.TypeReference;
import com.takesolution.framework.config.support.JsonMapper;

public class LoginAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    @Autowired
    private JsonMapper jsonMapper;

    private String PARAMETER_USER_ID = "p";

    private String PARAMETER_USER_PASSWORD = "c";

    public LoginAuthenticationFilter ( String loginAuthUrl,
            AuthenticationManager authenticationManager, AuthenticationSuccessHandler successHandler, AuthenticationFailureHandler failureHandler  ) {
        super( new AntPathRequestMatcher( loginAuthUrl ) );
        this.setAuthenticationManager( authenticationManager );
        this.setAuthenticationSuccessHandler( successHandler );
        this.setAuthenticationFailureHandler( failureHandler );
    }

    @Override
    public Authentication attemptAuthentication ( HttpServletRequest request, HttpServletResponse response ) throws AuthenticationException, IOException, ServletException {
        Map<String, String> parameter = jsonMapper.fromJson( request.getInputStream(), new TypeReference<Map<String, String>>(){} );
        String userId = parameter.get( PARAMETER_USER_ID );
        String password = parameter.get( PARAMETER_USER_PASSWORD );
        return new UsernamePasswordAuthenticationToken( userId, password );
    }
}
