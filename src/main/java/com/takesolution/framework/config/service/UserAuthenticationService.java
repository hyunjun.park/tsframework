package com.takesolution.framework.config.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.takesolution.framework.config.security.LoginUserDetails;
import com.takesolution.framework.config.service.dao.UserAuthenticationDao;
import com.takesolution.framework.config.vo.UserAuthentication;

public class UserAuthenticationService implements UserDetailsService {
    private UserAuthenticationDao userAuthenticationDao;

    public UserAuthenticationService ( UserAuthenticationDao userAuthenticationDao ) {
        this.userAuthenticationDao = userAuthenticationDao;
    }

    @Override
    public UserDetails loadUserByUsername ( String userId ) throws UsernameNotFoundException {
        UserAuthentication userAuthentication = userAuthenticationDao.findUserByUserId( userId );
        return LoginUserDetails.builder()
            .userId( userAuthentication.getUserId() )
            .pwd( userAuthentication.getPassword() )
            .build();
    }
}
