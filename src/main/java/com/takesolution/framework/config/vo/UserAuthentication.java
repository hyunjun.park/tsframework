package com.takesolution.framework.config.vo;

import lombok.Data;

@Data
public class UserAuthentication {
    private String userId;

    private String password;
}
