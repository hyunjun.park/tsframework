package com.takesolution.framework.config.support;

import java.io.InputStream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonMapper extends ObjectMapper {
    private static final long serialVersionUID = -3576088144729271080L;

    public JsonMapper () {
        DefaultPrettyPrinter prettyPrinter = new DefaultPrettyPrinter();
        DefaultPrettyPrinter.Indenter indenter = new DefaultIndenter( "    ", DefaultIndenter.SYS_LF );
        prettyPrinter.indentArraysWith( indenter );
        prettyPrinter.indentObjectsWith( indenter );

        enable(
            SerializationFeature.INDENT_OUTPUT
        ).disable(
            DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
            DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
        ).setDefaultPrettyPrinter( prettyPrinter );
    }

    public String toJson ( Object domain ) {
        try {
            return writeValueAsString( domain );
        } catch ( JsonProcessingException e ) {
            throw new RuntimeException( e );
        }
    }


    public <T> T fromJson ( InputStream jsonStr, Class<T> klass ) {
        try {
            return readValue( jsonStr, klass );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }

    public <T> T fromJson ( String jsonStr, Class<T> klass ) {
        try {
            return readValue( jsonStr, klass );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }

    public <T> T fromJson ( InputStream is, TypeReference<T> typeReference ) {
        try {
            return readValue( is, typeReference );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }

    public <T> T fromJson ( String jsonStr, TypeReference<T> typeReference ) {
        try {
            return readValue( jsonStr, typeReference );
        } catch ( Exception e ) {
            throw new RuntimeException( e );
        }
    }
}
