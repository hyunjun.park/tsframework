package com.takesolution.framework.config.provider;

import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoginAuthenticationProviderManager implements AuthenticationProvider {
    private List<AuthenticationProvider> providers;

    public LoginAuthenticationProviderManager ( List<AuthenticationProvider> providers ) {
        this.providers = providers;
    }

    @Override
    public boolean supports ( Class<?> klass ) {
        return true;
    }

    @Override
    public Authentication authenticate ( Authentication unconfirmedAuthentication ) throws AuthenticationException {
        Authentication authorized = unconfirmedAuthentication;
        for ( AuthenticationProvider provider : providers ) {
            log.debug( "authentication: {}", authorized.getClass() );
            if ( provider.supports( authorized.getClass() ) ) {
                authorized = provider.authenticate( authorized );
            }
        }
        return authorized;
    }
}
