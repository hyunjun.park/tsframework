package com.takesolution.framework.config.provider;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.takesolution.framework.config.security.LoginAuthentication;
import com.takesolution.framework.config.security.LoginUserDetails;
import com.takesolution.framework.config.security.Menu;
import com.takesolution.framework.config.support.JsonMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j( topic = "\n" )
public class LoginUserIdAndPasswordAuthenticationProvider implements AuthenticationProvider {
    @Resource( name = "jsonMapper" )
    private JsonMapper jsonMapper;

    private UserDetailsService userDetailsService;

    public LoginUserIdAndPasswordAuthenticationProvider ( UserDetailsService userDetailsService ) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public boolean supports ( Class<?> klass ) {
        return UsernamePasswordAuthenticationToken.class.equals( klass );
    }

    @Override
    public Authentication authenticate ( Authentication unconfirmedAuthentication ) throws AuthenticationException {
        UsernamePasswordAuthenticationToken unconfirmedToken = ( UsernamePasswordAuthenticationToken ) unconfirmedAuthentication;
        log.debug( "\n{}", jsonMapper.toJson( unconfirmedToken ) );
        String unconfirmedUserId = ( String ) unconfirmedToken.getPrincipal();
        String unconfirmedPassword = ( String ) unconfirmedToken.getCredentials();

        LoginUserDetails details = ( LoginUserDetails ) userDetailsService.loadUserByUsername( unconfirmedToken.getName() );
        String userId = details.getUserId();
        if ( !userId.equals( unconfirmedUserId ) ) {
            throw new UsernameNotFoundException( "사용자를 찾을 수 없습니다." );
        }
        String password = details.getPassword();
        if ( !password.equals( unconfirmedPassword ) ) {
            throw new BadCredentialsException( "비밀번호가 일치하지 않습니다." );
        }
        List<Menu> menus = new ArrayList<>();
        menus.add( Menu.of( "M1001", "메뉴-1" ) );
        menus.add( Menu.of( "M1002", "메뉴-2" ) );
        menus.add( Menu.of( "M1003", "메뉴-3" ) );
        details.setMenus( menus );
        return createSuccessAuthentication( details );
    }

    private Authentication createSuccessAuthentication ( UserDetails details ) {
        LoginAuthentication authentication = LoginAuthentication.of( details );
        authentication.setAuthenticated( true );
        log.debug( "\n{}", jsonMapper.toJson( authentication ) );
        return authentication;
    }
}
