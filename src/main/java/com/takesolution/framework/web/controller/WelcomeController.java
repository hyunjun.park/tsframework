package com.takesolution.framework.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.takesolution.framework.config.support.JsonMapper;
import com.takesolution.framework.config.util.SessionUtil;

@RestController
public class WelcomeController {
    @Autowired
    private JsonMapper jsonMapper;

    @GetMapping( "/" )
    public ModelAndView index ( @ModelAttribute ModelAndView mv ) {
        mv.setViewName( "index" );
        return mv;
    }

    @GetMapping( "/login" )
    public ModelAndView login ( @ModelAttribute ModelAndView mv, HttpServletResponse response ) throws IOException {
        if ( SessionUtil.isLogin() ) {
            response.sendRedirect( "/" );
        }
        return mv;
    }

    @GetMapping( value = "/session", produces = "application/javascript;charset=UTF-8" )
    public String session () {
        String strsession = jsonMapper.toJson( SessionUtil.getAuthentication() );
        return String.join( "", "define([],function(){return ", strsession, ";})" );
    }
}
