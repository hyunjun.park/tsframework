package com.takesolution.framework.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.takesolution.framework.config.support.JsonMapper;
import com.takesolution.framework.config.util.SessionUtil;

@RestController
public class TestController {
    @Autowired
    private JsonMapper jsonMapper;

    @GetMapping( "/test" )
    public ModelAndView test ( @ModelAttribute ModelAndView mv ) {
        return mv;
    }

}
