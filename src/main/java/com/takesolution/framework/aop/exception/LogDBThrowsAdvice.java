package com.takesolution.framework.aop.exception;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogDBThrowsAdvice {
    private Logger log = LoggerFactory.getLogger( getClass() );

    public void afterThrowing ( Exception ex ) throws Throwable {
        log.debug("***");
        log.debug("Generic Exception Capture");
        log.debug("Caught: " + ex.getClass().getName());
        log.debug("***\n");
    }

    public void afterThrowing ( Method method, Object[] args, Object target, IllegalArgumentException ex ) throws Throwable {
        log.debug("***");
        log.debug("IllegalArgumentException Capture");
        log.debug("Caught: " + ex.getClass().getName());
        log.debug("Method: " + method.getName());
        log.debug("***\n");
    }
}