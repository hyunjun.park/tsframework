package com.takesolution.framework.system.cms.service.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.takesolution.framework.config.annotation.Mapper;

@Mapper
public interface Cms001Dao {
	
    List<Map<String, Object>> selectTestList ( @Param( "param1" ) String param1 );
}
