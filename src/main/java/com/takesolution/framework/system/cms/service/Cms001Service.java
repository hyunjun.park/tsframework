package com.takesolution.framework.system.cms.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.takesolution.framework.system.cms.service.dao.Cms001Dao;

@Service
public class Cms001Service {
	
    @Autowired
    private Cms001Dao cms001Dao;

    public List<Map<String, Object>> selectTestList ( Map<String, Object> vo ) {
        String param1 = ( String ) vo.get( "param1" );
        return cms001Dao.selectTestList( param1 );
    }
}
