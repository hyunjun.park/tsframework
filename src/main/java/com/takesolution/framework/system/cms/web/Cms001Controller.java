package com.takesolution.framework.system.cms.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.takesolution.framework.system.cms.service.Cms001Service;

@RequestMapping( "/cms001" )
@RestController
public class Cms001Controller {
	
    @Autowired
    private Cms001Service cms001Service;

    @PostMapping( "/selectTestList" )
    public List<Map<String, Object>> selectTestList ( @RequestBody Map<String, Object> param ) {
        return cms001Service.selectTestList( param );
    }
    

}
